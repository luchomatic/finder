<?php

use Illuminate\Database\Seeder;
use App\Habitacion;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);

		DB::table('users')->insert([
			'name' => 'cliente',
			'email' => 'cliente@gmail.com',
			'password' => bcrypt('secret'),
		]);

		$servicios = [];
		$servicios[] = [
			'tipo' => 'auto',
			'descripcion' => 'Auto marca Dodge, capacidad 4 personas',
			'precio' => 100
		];

		$servicios2[] = [
			'tipo' => 'avion',
			'descripcion' => 'Avion de Areolinas argentinas, 2 personas, de Argentina a España',
			'precio' => 300
		];

		DB::table('ordens')->insert([
			'servicios' => serialize($servicios),
			'total' => 200,
            'user_id' => 1,
            'status' => 1,
            'fecha' => date("Y/m/d"),
		]);
		DB::table('ordens')->insert([
			'servicios' => serialize($servicios2),
			'total' => 400,
			'user_id' => 1,
			'status' => 1,
			'fecha' => date("Y/m/d"),
		]);

		$habitaciones = Habitacion::all();
		foreach ($habitaciones as $key => $habitacion) {
			$habitacion->hotel = 'Hotel '.$key;
			$habitacion->save();
		}
    }
}
