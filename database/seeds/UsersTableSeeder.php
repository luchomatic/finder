<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$users = factory(App\User::class, 30)->create();
		$users = factory(App\Auto::class, 30)->create();
		$users = factory(App\Vuelo::class, 30)->create();
		$users = factory(App\Habitacion::class, 30)->create();
	}
}
