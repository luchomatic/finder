<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Auto::class, function (Faker $faker) {
    static $password;

    return [
        'capacidad' => rand(2,5),
        'marca' => $faker->company,
        'ciudad' => $faker->city,
        'precio_dia' => $faker->randomFloat(null, 10, 100)
    ];
});
