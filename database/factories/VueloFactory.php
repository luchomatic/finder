<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Vuelo::class, function (Faker $faker) {
    static $password;

    return [
        'fecha' => $faker->date(),
        'origen' => $faker->city,
        'destino' => $faker->city,
        'precio' => $faker->randomFloat(null, 10, 100)
    ];
});
