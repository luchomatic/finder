<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
Route::get('/', function () {
    return view('index');
});

Route::get('/autos', function () {
    $autos = App\Auto::all();
    $filter = false;
    if (!empty($_GET['ciudad'])){
        $autos = $autos->filter(function ($value, $key) {
            return strtolower($value->ciudad) == strtolower($_GET['ciudad']);
        });
        $filter = true;
    }

    if (!empty($_GET['marca'])){
        $autos = $autos->filter(function ($value, $key) {
            return strtolower($value->marca) == strtolower($_GET['marca']);
        });
        $filter = true;
    }

    if (!empty($_GET['capacidad'])){
        $autos = $autos->filter(function ($value, $key) {
            // return strtolower($value->capacidad) >= strtolower($_GET['capacidad']);
            return $value->capacidad == $_GET['capacidad'];
        });
        $filter = true;
    }

    return view('auto', [
        'autos' => $autos,
        'filter' => $filter
    ]);
});

Route::get('/habitaciones', function () {
	$habitaciones = App\Habitacion::all();

    $filter = false;

    if (!empty($_GET['ciudad_origen'])){
        $habitaciones = $habitaciones->filter(function ($value, $key) {
            return strtolower($value->ciudad) == strtolower($_GET['ciudad_origen']);
        });
        $filter = true;
    }

    if (!empty($_GET['hotel'])){
        $filter = true;
        $habitaciones = $habitaciones->filter(function ($value, $key) {
            return strtolower($value->hotel) == strtolower($_GET['hotel']);
        });
    }

    if (!empty($_GET['fecha_inicio'])){     
        $filter = true;
        $habitaciones = $habitaciones->filter(function ($value, $key) {
            return strtotime($value->created_at) >= strtotime(date($_GET['fecha_inicio']));
        });
    }

    if (!empty($_GET['fecha_fin'])){
        $filter = true;
        $habitaciones = $habitaciones->filter(function ($value, $key) {
            return strtotime($value->created_at) <= strtotime(date($_GET['fecha_fin']));
        });
    }

    $habitaciones = $habitaciones->sortBy('precio_noche');

	return view('habitacion', [
		'habitaciones' => $habitaciones,
        'filter' => $filter
	]);
});

Route::get('/vuelos', function () {
	$vuelos = App\Vuelo::all();
	$filter = false;

	if (!empty($_GET['ciudad_origen'])){
        $vuelos = $vuelos->filter(function ($value, $key) {
            return strtolower($value->origen) == strtolower($_GET['ciudad_origen']);
        });
        $filter = true;
    }

    if (!empty($_GET['ciudad_destino'])){
    	$filter = true;
        $vuelos = $vuelos->filter(function ($value, $key) {
            return strtolower($value->destino) == strtolower($_GET['ciudad_destino']);
        });
    }

    if (!empty($_GET['fecha_inicio'])){    	
    	$filter = true;
        $vuelos = $vuelos->filter(function ($value, $key) {
            return strtotime($value->fecha) >= strtotime(date($_GET['fecha_inicio']));
        });
    }

    if (!empty($_GET['fecha_fin'])){
    	$filter = true;
        $vuelos = $vuelos->filter(function ($value, $key) {
            return strtotime($value->fecha) <= strtotime(date($_GET['fecha_fin']));
        });
    }

    $vuelos = $vuelos->sortBy('precio');

	return view('vuelo', [
		'vuelos' => $vuelos,
		'filter' => $filter		
	]);
});

Route::get('/carrito', function () {
	$carrito = Cart::content();
	$precio_total = 0;
	foreach ($carrito as $producto){
		$precio_total += $producto->price;
	}
	return view('carrito', [
		'carrito' => $carrito,
		'precio_total' => $precio_total
	]);
});

Route::get('/ordenes', function () {

    if (Auth::check()) {
        // The user is logged in...
        $id = Auth::id();
        $ordenes = App\Orden::where('user_id', $id)->where('status',1)->get();        
        
        foreach ($ordenes as $key => $orden) {
            $orden->servicios =  unserialize($orden->servicios);
        }
        
        return view('ordenes', [
            'ordenes' => $ordenes
        ]);
    }else{
        return redirect()->route('login');
    }
});

Route::post('/ordenes/delete',function (Request $request){
    $id = (int)$request->input('id');
    $orden = App\Orden::find($id);
    $orden->status = 0;
    $orden->save();
    return response()->json([]);


});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/addToCart/auto/{id}', function ($id) {
	$auto = App\Auto::where('id', $id)->first();
	Cart::add($id, $auto->marca, 1, $auto->precio_dia, ['tipo' => 'auto', 'capacidad' => $auto->capacidad, 'ciudad' => $auto->ciudad]);

	$carrito = Cart::content();
	$precio_total = Cart::total();
	return view('carrito', [
		'carrito' => $carrito,
		'precio_total' => $precio_total
	]);
});
Route::get('/addToCart/vuelo/{id}', function ($id) {
	$vuelo = App\Vuelo::where('id', $id)->first();
	Cart::add($id, $id, 1, $vuelo->precio, ['tipo' => 'vuelo', 'origen' => $vuelo->origen, 'destino' => $vuelo->destino, 'fecha' => $vuelo->fecha]);

	$carrito = Cart::content();
	$precio_total = Cart::total();
	return view('carrito', [
		'carrito' => $carrito,
		'precio_total' => $precio_total
	]);
});
Route::get('/addToCart/habitacion/{id}', function ($id) {
	$habitacion = App\Habitacion::where('id', $id)->first();
	Cart::add($id, $id, 1, $habitacion->precio_noche, ['tipo' => 'habitacion', 'ciudad' => $habitacion->ciudad, 'capacidad' => $habitacion->capacidad]);

	$carrito = Cart::content();
	$precio_total = Cart::total();
	return view('carrito', [
		'carrito' => $carrito,
		'precio_total' => $precio_total
	]);
});

Route::get('/comprar', function () {
	if (Auth::check()) {
		// The user is logged in...
		$id = Auth::id();
		$orden = new App\Orden();
		$orden->user_id = $id;
		$orden->total = Cart::total();
		$orden->fecha = date('Y-m-d');
		$carrito = Cart::content();
		$servicios = [];
		foreach ($carrito as $producto){
			switch ($producto->options['tipo']) {
				case 'auto':
					$descripcion = 'Auto de capacidad'. $producto->options['capacidad'] . '. Ciudad '. $producto->options['ciudad']. '. Precio por semana '. $producto->price;
					break;
				case 'vuelo':
					$descripcion = 'Vuelo desde '. $producto->options['origen'] . ' hasta '. $producto->options['destino']. '. Fecha '. $producto->fecha. 'Precio '. $producto->price;
					break;
				case 'habitacion':
					$descripcion = 'Habitacion de capacidad '. $producto->options['capacidad'] . '. Ciudad '. $producto->options['ciudad']. '. Precio '. $producto->price;
					break;
			}

			$servicios[] = [
				'tipo' => $producto->options['tipo'],
				'descripcion' => $descripcion,
				'precio' => $producto->precio
			];
		}
		$orden->servicios = serialize($servicios);
		$orden->save();
		$ordenes = App\Orden::where('user_id', $id)->get();
        foreach ($ordenes as $key => $orden) {
            $orden->servicios =  unserialize($orden->servicios);
        }
		return view('ordenes', [
			'ordenes' => $ordenes
		]);
	}else{
		return redirect()->route('login');
	}
});

