Clonar el repo
correr comando composer install

Correr el servicio MySQL de tu computador (por ejemplo usando Xampp)
Crear una base de datos que se llame "finder"

Copiar el archivo .env.example y pegarlo en la raiz con el nombre .env
en ese archivo .env
Modificar estas 3 lineas para que el puerto, el usuario y la contraseņa coincidan con los de tu instancia de MySQL

DB_PORT=3306
DB_USERNAME=root
DB_PASSWORD=

correr comando php artisan migrate:refresh --seed
correr comando php artisan key:generate
correr compando php artisan serve
ir a localhost:8000 en chrome


Para el carrito usamos esta libreria: https://github.com/Crinsane/LaravelShoppingcart#usage