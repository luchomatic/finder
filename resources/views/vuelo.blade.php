@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="card">                
                
                <div class="body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                <div class="fn-title">
                    <button type="button" class="btn btn-default btn-circle waves-effect" onclick="location.href = 'vuelos';">
                        <i class="material-icons">flight</i>
                        
                    </button>
                    <span class="title">Encuentra <strong>Vuelos</strong></span>
                </div>
                    <div class="row clearfix">
                        
                        <form method="GET" action="/vuelos">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="ciudad_origen" id="ciudad_origen" class="form-control" placeholder="Ciudad Origen" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="ciudad_destino" id="ciudad_destino" placeholder="Ciudad Destino" />
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                    <label for="fecha_inicio">Fecha Partida <small>(opcional)</small></label>
                                        <input class="form-control" name="fecha_inicio" id="fecha_inicio" type="date">                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="fecha_fin">Fecha Regreso <small>(opcional)</small></label>
                                        <input type="date" class="form-control" name="fecha_fin" id="fecha_fin" >
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <button type="submit" class="btn bg-orange btn-block btn-lg waves-effect" id="buscar"><i class="material-icons">search</i></button>
                            </div>       
                            @if($filter)
                            <div class="col-sm-12">                 
                                <button type="button" class="btn btn-default btn-block btn-lg waves-effect" onclick="v.all()">VER TODOS</button>
                            </div>
                            @endif

                        </form>                
                    </div>

                 </div>
             </div>
         </div>
     </div>
     <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body table-responsive">
                    <table class="table table-hover">
                        <thead class="bg-orange">
                            <tr>                                
                                <th>Origen</th>
                                <th>Destino</th>
                                <th>Fecha</th>
                                <th>Precio</th>
                                <th>-</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($vuelos as $vuelo)                            
                                <tr>                                    
                                    <td>{{$vuelo->origen}}</td>
                                    <td>{{$vuelo->destino}}</td>
                                    <td>{{$vuelo->fecha}}</td>
                                    <td>${{$vuelo->precio}}</td>
                                    <td>                                       
                                        @if (auth()->check())
                                            <a href="/addToCart/vuelo/{{$vuelo->id}}">Agregar al carrito</a>
                                        @endif
                                    </td>
                                </tr>                                                    
                           @endforeach 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
     </div>        
 </div>
 @endsection
<!-- @section('javascripts') -->
<script type="text/javascript" src="{{asset('js/vuelo.js')}}"></script>
<!-- @endsection -->