@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
               <div class="body">
                    <div class="fn-title">
                        <button type="button" class="btn btn-default btn-circle waves-effect" onclick="location.href = 'ordenes';">
                            <i class="material-icons">card_travel</i>
                            
                        </button>
                        <span class="title">Tus <strong>Órdenes</strong></span>
                    </div>
                    @if(count($ordenes) == 0)
                        No dispone de ordenes 
                    @endif
                    
                                                    
                    <div class="panel-group" id="accordion_1" role="tablist" aria-multiselectable="true">
                    @foreach ($ordenes as $orden)
                        <div class="panel panel-success" id="{{$orden->id}}">
                            <div class="panel-heading" role="tab" id="headingOne_2">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_{{$orden->id}}" aria-expanded="true" aria-controls="collapseOne_{{$orden->id}}">
                                        <strong>Orden:</strong>  #{{ $orden->id }}: ${{ $orden->total}}
                                        <span class="badge bg-orange right" onclick="v.deleteItem({{$orden->id}})">                    
                                            <i class="material-icons">delete</i>
                                        </span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne_{{$orden->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_{{$orden->id}}">
                                <div class="panel-body">
                                    @foreach($orden->servicios as $serv)
                                        <p>{{$serv['descripcion']}}</p>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>        
                                       
                    <!-- </ul> -->
                </div>
                
        </div>    
    </div>
</div>
@endsection
@section('javascripts')
<script type="text/javascript" src="{{asset('js/vuelo.js')}}"></script>
@endsection