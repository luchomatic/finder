@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="card">                
                
                <div class="body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="fn-title">
                        <button type="button" class="btn btn-default btn-circle waves-effect" onclick="location.href = 'habitaciones';">
                            <i class="material-icons">hotel</i>
                            
                        </button>
                        <span class="title">Encuentra <strong>Habitaciones</strong></span>
                    </div>
                    <div class="row clearfix">
                        
                        <form method="GET" action="/habitaciones">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="ciudad_origen" id="ciudad_origen" class="form-control" placeholder="Ciudad" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="hotel" id="hotel" placeholder="Hotel" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                    <label for="fecha_inicio">Fecha Entrada <small>(opcional)</small></label>
                                        <input class="form-control" name="fecha_inicio" id="fecha_inicio" type="date">                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="fecha_fin">Fecha Salida <small>(opcional)</small></label>
                                        <input type="date" class="form-control" name="fecha_fin" id="fecha_fin" >
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <button type="submit" class="btn bg-orange btn-block btn-lg waves-effect" id="buscar"><i class="material-icons">search</i></button>
                            </div>       
                            @if($filter)                 
                                <button type="button" onclick="v.allHab()">VER TODOS</button>
                            @endif
                        </form>                
                    </div>

                 </div>
             </div>
         </div>
     </div>
     <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body table-responsive">
                    <table class="table table-hover">
                        <thead class="bg-orange">
                            <tr>                                
                                <th>Habitacion</th>
                                <th>Hotel</th>
                                <th>Ciudad</th>
                                <th>Precio</th>
                                <th>Fecha</th>
                                <th>-</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($habitaciones) == 0)
                                No dispone de ordenes 
                            @endif
                            @foreach ($habitaciones as $habitacion)                                                         
                                <tr>                                    
                                    <td>Habitacion {{$habitacion->id}}</td>
                                    <td>{{$habitacion->hotel}}</td>
                                    <td>{{$habitacion->ciudad}}</td>
                                    <td>${{$habitacion->precio_noche}}</td>
                                    <td>{{$habitacion->created_at->format('Y-m-d')}}</td>
                                    <td>                                       
                                        @if (auth()->check())
                                            <a href="/addToCart/habitacion/{{$habitacion->id}}">Agregar al carrito</a>
                                        @endif
                                    </td>
                                </tr>                                                                               
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
     </div>        
 </div>
 @endsection
@section('javascripts')
<script type="text/javascript" src="{{asset('js/vuelo.js')}}"></script>
@endsection
