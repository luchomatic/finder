@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="card">                
                
                <div class="body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="fn-title">
                        <button type="button" class="btn btn-default btn-circle waves-effect" onclick="location.href = 'autos';">
                            <i class="material-icons">directions_car</i>
                            
                        </button>
                        <span class="title">Encuentra <strong>Autos</strong></span>
                    </div>
                    <div class="row clearfix">
                        
                        <form method="GET" action="/autos">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="ciudad" id="ciudad" class="form-control" placeholder="Ciudad" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="marca" id="marca" placeholder="Marca" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" class="form-control" name="capacidad" id="capacidad" placeholder="Capacidad" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <button type="submit" class="btn bg-orange btn-block btn-lg waves-effect" id="buscar"><i class="material-icons">search</i></button>
                            </div>
                            @if($filter)                 
                                <button type="button" onclick="v.allCars()">VER TODOS</button>
                            @endif                        
                        </form>                
                    </div>

                 </div>
             </div>
         </div>
     </div>
     <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body table-responsive">
                    <table class="table table-hover">
                        <thead class="bg-orange">
                            <tr>                                
                                <th>Marca</th>
                                <th>Capacidad</th>
                                <th>Ciudad</th>
                                <th>-</th>
                            </tr>
                        </thead>
                        <tbody>
                                                    

                            @foreach($autos as $auto)
                                <tr>                                    
                                    <td>{{$auto->marca}}</td>
                                    <td>{{$auto->capacidad}}</td>
                                    <td>{{$auto->ciudad}}</td>
                                    <td>                                       
                                        @if (auth()->check())
                                            <a href="/addToCart/auto/{{$auto->id}}">Agregar al carrito</a> </p>
                                        @endif
                                    </td>
                                </tr>
                                    


                             @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
     </div>        
 </div>
 @endsection
@section('javascripts')
<script type="text/javascript" src="{{asset('js/vuelo.js')}}"></script>
@endsection