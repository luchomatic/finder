@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Mi Carrito</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                        @foreach ($carrito as $producto)
                            <p>Este es el producto {{ $producto->name }} del tipo: {{ $producto->options['tipo'] }} y el precio es ${{ $producto->price }}</p>
                        @endforeach
                    <p>Precio total: ${{$precio_total}}</p>

                    <h3>Datos de la tarjeta</h3>
                    <label for="">Nombre</label><br><input type="text"><br>
                    <label for="">Numero</label><br><input type="number"><br>
                    <label for="">Codigo de seguridad</label><br><input type="number"><br>
                    <label for="">Fecha de vencimiento</label><br><input type="number"><br>
                    <a href="/comprar">Comprar</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
