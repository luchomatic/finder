﻿@extends('layouts.admin')
@section('content')
<div class="container home">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <img class="logo" src="{{ asset('images/logo.png')}}" alt="">
            <h1>Dejá de buscar. Encontrá lo que querés</h1>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-8 col-md-offset-2">
    		<div class="row nav-links">
    			<div class="col-md-4">
    				<button type="button" class="btn btn-default btn-circle waves-effect" onclick="location.href = 'vuelos';">
                        <i class="material-icons">flight</i>
                        
                    </button>
                    <span class="title">Encuentra <strong>Vuelos</strong></span>
    			</div>
    			<div class="col-md-4">
    				<button type="button" class="btn btn-default btn-circle waves-effect" onclick="location.href = 'autos';">
                        <i class="material-icons">directions_car</i>
                        
                    </button>
                    <span class="title">Alquila <strong>Autos</strong></span>
    			</div>
    			<div class="col-md-4">
    				<button type="button" class="btn btn-default btn-circle waves-effect" onclick="location.href = 'habitaciones';">
                        <i class="material-icons">hotel</i>
                        
                    </button>
                    <span class="title">Encuentra <strong>Habitaciones</strong></span>
    			</div>
    		</div>
    	</div>
    </div>
</div>
@endsection