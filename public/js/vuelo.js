class Vuelo{
	constructor(){

	}

	all(){
		return location.href = '/vuelos';
	}

	allHab(){
		return location.href = '/habitaciones';	
	}

	allCars(){
		return location.href = '/autos';
	}
	
	deleteItem(id){
		if(confirm('¿ Desea eliminar la orden ?')){

			return $.ajax({
				 headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
				url: '/ordenes/delete',
				type : 'post',
				data : {id : id},
				success: (response)=>{
					console.log('response',response);
					document.getElementById(id).remove();
				},
				error: (response)=>{

				}
			})
		}
	}	
}

var v = new Vuelo;